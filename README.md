# sigproc

Notebook-based signal processing module.

## FEATURES

* Load/save signal in wav format
* Manipulate signals in both time and frequency domains
* Visualize signal in both time and frequency domains
